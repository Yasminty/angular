import { Component, OnInit } from '@angular/core';

@Component({
  selector: '[app-test]',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
  onClick(event) {
    console.log(event.type);

  }
  logmessage(myinput){
    console.log(myinput);

  }

}
